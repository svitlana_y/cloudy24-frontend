import './Loans.css';
import React, { Component } from 'react';
import UnderConstruction from '../underConstruction/UnderConstruction';

function Loans() {
  return (
    <div className="loans">
      <UnderConstruction section={"Кредитування"} />
    </div>
  );
}

export default Loans;
